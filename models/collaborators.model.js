const { Schema, model } = require("mongoose");

const categorySchema = new Schema(
  {
    name: {
        type: String,
        required: true,
        unique: true,
        uppercase: true,
        trim: true
    },
    phone1: {
        type: Number,
        unique: true,
        required: true,
        trim: true,
        maxlength:9
    },
    phone2: {
        type: Number,
        unique: true,
        trim: true,
        maxlength:9
    },
    email: {
        type: String,
        unique: true,
        trim: true
    },
    address: {
        type: String,
        required: true,
        unique: true,
        trim: true
    }
  },
  { timestamps: true }
);

module.exports = model("Collaborators", categorySchema);
//recordar colocar ID usuario como referencia
