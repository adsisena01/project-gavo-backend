const { Schema, model } = require('mongoose');

const productCartSchema = new Schema({
    product: {
        type: Schema.Types.ObjectId,
        ref: "Product"
    }
});
const ProductCart = model('ProductCart', productCartSchema);


const orderSchema = new Schema({
    products: [productCartSchema],
    transaction_id: {},
    amount: { type: Number },
    address: String,
    status: {
        type: String,
        default: "Recieved",
        enum: ["Cancelled", "Delivered", "Shipped", "Processing", "Recieved"]
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "User"
    }
},
    { timestamps: true }

);
const Order = model('Order', orderSchema);

module.exports = { Order, ProductCart };