const { Schema, model } = require("mongoose");

const stockSchema = new Schema(
    {
        cust_id: {
            type: String,
            required: [true, 'El stock debe tener un identificador'],
            maxlength: 7,

        },
        name: {
            type: String,
            required: [true, 'El nombre en el stock debe tener un nombre unico'],
            uppercase: true,
            unique: true,
            maxlength: 15,
            minlength: 3
        },
        quantity: {
            type: Number,
            required: [true, 'La cantidad en el Stock es requerida'],
            max: 500
        },
        imageUrl: {
            type: String,
            require: true,
            trim: true
        },



    },
    { timestamps: true }
);

module.exports = model("Stock", stockSchema);
