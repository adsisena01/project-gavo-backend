const { Schema, model } = require("mongoose");

const userSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      max: 100
    },
    email: {
      type: String,
      required: true,
      lowercase: true,
      unique: true,
      trim: true

    },
    role: {
      type: String,
      default: "user",
      enum: ["user", "admin", "superadmin"]
    },
    username: {
      type: String,
      required: true,
      unique: true,
      max: 50
    },
    password: {
      type: String,
      required: true
    },
    cart: {
      items: [
        {
          productId: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
          quantity: { type: Number, required: true }
        }
      ]
    }
  },
  { timestamps: true }
);

module.exports = model("User", userSchema);
