const { Schema, model } = require("mongoose");

const categorySchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true,
      uppercase: true,
      trim: true
    },
    cust_id: {
      type: String,
      required: [true, 'El stock debe tener un identificador'],
      maxlength: 5,
    },
    imageUrl: {
      type: String,
      required: true
    }
  },
  { timestamps: true }
);

module.exports = model("Category", categorySchema);
//recordar colocar ID usuario como referencia
