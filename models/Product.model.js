const { Schema, model } = require("mongoose");


const productSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    price: {
      type: Number,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    imageUrl: {
      type: String,
      require: true
    },
    category: {
      type: Schema.Types.ObjectId,
      ref: "Category",
      required: true
    },
    ingredients: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Stock'
      }
    ]

  },
  { timestamps: true }
);

module.exports = model("Product", productSchema);
//recordar colocar ID usuario como referencia
