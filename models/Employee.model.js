const { Schema, model } = require("mongoose");

const categorySchema = new Schema(
  {
    name: {
        type: String,
        required: true,
        unique: true,
        uppercase: true,
        trim: true
    },
    document: {
        type: Number,
        required: true,
        unique: true,
        trim: true,
        maxlength:9
    },
    phone: {
        type: Number,
        required: true,
        unique: true,
        trim: true,
        maxlength:9
    },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    eps: {
        type: String,
        required: true,
        unique: true,
        trim: true
    }
  },
  { timestamps: true }
);

module.exports = model("Employee", categorySchema);
//recordar colocar ID usuario como referencia
