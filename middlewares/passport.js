const User = require('../models/User.model')
const passportJWT = require('passport-jwt')
const { SECRET } = require('../config')

let jwtOptions = {
  secretOrKey: SECRET,
  jwtFromRequest: passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken()
}

module.exports = new passportJWT.Strategy(jwtOptions, async (jwtPayload, done) => {
  await User.findById(jwtPayload.user_id)
    .then(user => {
      if (user) {
        return done(null, user);
      }
      return done(null, false);
    })
    .catch(err => {
      return done(null, false);
    });
})