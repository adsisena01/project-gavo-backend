// include product model
const Employee = require("../models/Employee.model");

// create a new Product.
exports.employee_create = function (req, res) {
  //.............................. validate request.......................................................
  if (!req.body.name || !req.body.document || !req.body.phone || !req.body.email || !req.body.eps) {
    return res.status(400).send({
      success: false,
      message: "Please enter employee name, document, phone, email, and eps"
    });
  }

  // create a employee
  let employee = new Employee(
    ({ name, document, phone, email, eps } = req.body)
  );

  //............................save employee in the database.........................................................
  employee
    .save()
    .then(data => {
      res.send({
        success: true,
        message: "Employee successfully created",
        data: data
      });
    })
    .catch(err => {
      res.status(500).send({
        success: false,
        message:
          err.message || "Some error occurred while creating the employee.",
      });
      console.log(err)
    });
};

//......................... retrieve and return all employee.................................................................
exports.all_employee = (req, res) => {
  Employee.find()
    .then(data => {
      var message = "";
      if (data === undefined || data.length == 0) message = "No employee found!";
      else message = "Employees successfully retrieved";

      res.send({
        success: true,
        message: message,
        data: data
      });
    })
    .catch(err => {
      res.status(500).send({
        success: false,
        message: err.message || "Some error occurred while retrieving employees."
      });
    });
};

// find a single employee with a id.
exports.employees_details = (req, res) => {
  Employee.findById(req.params.id)
    .then(data => {
      if (!data) {
        return res.status(404).send({
          success: false,
          message: "Employee not found with id " + req.params.id
        });
      }
      res.send({
        success: true,
        message: "Employee successfully retrieved",
        data: data
      });
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          success: false,
          message: "Employee not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        success: false,
        message: "Error retrieving employee with id " + req.params.id
      });
    });
};

// update a employee  by the id.
exports.employee_update = (req, res) => {
  // validate request
  if (!req.body.phone || !req.body.email) {
    return res.status(400).send({
      success: false,
      message: "Please enter employee phone and email"
    });
  }

  // find employee and update
  Employee.findOneAndUpdate(
    req.params.id,
    {
      $set: req.body
    },
    { new: true }
  )
    .then(data => {
      if (!data) {
        return res.status(404).send({
          success: false,
          message: "Employee not found with id " + req.params.id
        });
      }
      res.send({
        success: true,
        data: data
      });
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          success: false,
          message: "Employee not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        success: false,
        message: "Error updating employee with id " + req.params.id
      });
    });
};

// delete a employee with the specified id.
exports.employee_delete = (req, res) => {
  Employee.findOneAndDelete(req.params.id)
    .then(data => {
      if (!data) {
        return res.status(404).send({
          success: false,
          message: "Employee not found with id " + req.params.id
        });
      }
      res.send({
        success: true,
        message: "Employee successfully deleted!"
      });
    })
    .catch(err => {
      if (err.kind === "ObjectId" || err.name === "NotFound") {
        return res.status(404).send({
          success: false,
          message: "Employee not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        success: false,
        message: "Could not delete employee with id " + req.params.id
      });
    });
};

/* const productCtrl = {};
const { Product } = require("../models/product.model");

//const { userAuth } = require("../utils/Auth");

productCtrl.getProducts = async (req, res) => {
  const products = await Product.find();
  res.json(products);
};

module.exports = productCtrl; */

/* exports.getAddProduct = (req, res, next) => {
  res.render("admin/edit-product", {
    pageTitle: "Add Product",
    path: "/admin/add-product",
    editing: false,
    hasError: false,
    errorMessage: null,
    validationErrors: []
  });
};




router.post("/register-user", async (req, res) => {
    await userRegister(req.body, "user", res);
  });
 */

/* productCtrl.createProducts = async (req, res) => {
  const { title, price, description, imageUrl, userId } = req.body;
  const newProduct = new Product({
    title,
    price,
    description,
    imageUrl,
    userId
  });
  await newProduct.save();
  res.json("Nuevo producto agregado");
}; */
