// include category model
const Category = require("../models/category.model");

// create a new Category.
exports.category_create = function(req, res) {
  //.............................. validate request.......................................................
  if (!req.body.name || !req.body.cust_id || !req.body.imageUrl) {
    return res.status(400).send({
      success: false,
      message:
        "Upss!!! algo te falto, Por favor ingrese el nombre, el cust_id y la categoria"
    });
  }

  // create a category
  let category = new Category(({ name, cust_id, imageUrl } = req.body));

  //............................save product in the database.........................................................
  category
    .save()
    .then(data => {
      res.send({
        success: true,
        message: "Categoria creada satisfactoriamente",
        data: data
      });
    })
    .catch(err => {
      res.status(500).send({
        success: false,
        message:
          err.message || "Un error ha ocurrido mientras se creaba la categoria"
      });
    });
};

//......................... retrieve and return all products.................................................................
exports.all_category = (req, res) => {
  Category.find()
    .then(data => {
      var message = "";
      if (data === undefined || data.length == 0)
        message = "Categoria no encontrada!";
      else message = "Categoria recuperada con éxito";

      res.send({
        success: true,
        message: message,
        data: data
      });
    })
    .catch(err => {
      res.status(500).send({
        success: false,
        message:
          err.message || "Se produjo algún error al mostrar las categorias."
      });
    });
};

// find a single category with a id.
exports.category_details = (req, res) => {
  Category.findById(req.params.id)
    .then(data => {
      if (!data) {
        return res.status(404).send({
          success: false,
          message: "Categoría no encontrada por el id " + req.params.id
        });
      }
      res.send({
        success: true,
        message: "Categoria recuperada con exito",
        data: data
      });
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          success: false,
          message: "Categoría no encontrada con el id " + req.params.id
        });
      }
      return res.status(500).send({
        success: false,
        message:
          "Error de recuperación de la categoría con el id " + req.params.id
      });
    });
};

// update a category  by the id.
exports.category_update = (req, res) => {
  // validate request
  if (!req.body.name) {
    return res.status(400).send({
      success: false,
      message: "Por favor, introduzca el nombre de la categoria"
    });
  }

  // find category and update
  Category.findOneAndUpdate(
    req.params.id,
    {
      $set: req.body
    },
    { new: true }
  )
    .then(data => {
      if (!data) {
        return res.status(404).send({
          success: false,
          message: "Categoria no encontrada con el id " + req.params.id
        });
      }
      res.send({
        success: true,
        data: data
      });
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          success: false,
          message: "Categoria no encontrada con el id " + req.params.id
        });
      }
      return res.status(500).send({
        success: false,
        message: "Error actualizando la categoria con el id " + req.params.id
      });
    });
};

// delete a category with the specified id.
exports.category_delete = (req, res) => {
  Category.findOneAndDelete(req.params.id)
    .then(data => {
      if (!data) {
        return res.status(404).send({
          success: false,
          message: "Categoria no encontrada con el id " + req.params.id
        });
      }
      res.send({
        success: true,
        message: "Category successfully deleted!"
      });
    })
    .catch(err => {
      if (err.kind === "ObjectId" || err.name === "NotFound") {
        return res.status(404).send({
          success: false,
          message: "Categoria no encontrada con el id " + req.params.id
        });
      }
      return res.status(500).send({
        success: false,
        message: "No se pudo borrar la categoria con el id " + req.params.id
      });
    });
};
