// include category model
const Collaborators = require("../models/collaborators.model");

// create a new Category.
exports.collaborators_create = function(req, res) {
  //.............................. validate request.......................................................
  if (!req.body.name || !req.body.phone1 || !req.body.address) {
    return res.status(400).send({
      success: false,
      message:
        "Upss!!! algo te falto, Por favor ingrese el nombre, el telefono y la direccion"
    });
  }

  // create a category
  let collaborators = new Collaborators(({ name, phone1, phone2, email, address } = req.body));

  //............................save product in the database.........................................................
  collaborators
    .save()
    .then(data => {
      res.send({
        success: true,
        message: "Colaborador creado satisfactoriamente",
        data: data
      });
    })
    .catch(err => {
      res.status(500).send({
        success: false,
        message:
          err.message || "Un error ha ocurrido mientras se creaba el colaborador"
      });
    });
};

//......................... retrieve and return all products.................................................................
exports.all_collaborator = (req, res) => {
  Collaborators.find()
    .then(data => {
      var message = "";
      if (data === undefined || data.length == 0)
        message = "Colaborador no encontrado!";
      else message = "Colaborador recuperado con éxito";

      res.send({
        success: true,
        message: message,
        data: data
      });
    })
    .catch(err => {
      res.status(500).send({
        success: false,
        message:
          err.message || "Se produjo algún error al mostrar los colaboradores."
      });
    });
};

// find a single collaborators with a id.
exports.collaborators_details = (req, res) => {
  Collaborators.findById(req.params.id)
    .then(data => {
      if (!data) {
        return res.status(404).send({
          success: false,
          message: "Colaborador no encontrado por el id " + req.params.id
        });
      }
      res.send({
        success: true,
        message: "Colaborador recuperado con exito",
        data: data
      });
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          success: false,
          message: "Colaborador no encontrado con el id " + req.params.id
        });
      }
      return res.status(500).send({
        success: false,
        message:
          "Error de recuperación del colaborador con el id " + req.params.id
      });
    });
};

// update a collaborator  by the id.
exports.collaborator_update = (req, res) => {
  // validate request
  if (!req.body.name) {
    return res.status(400).send({
      success: false,
      message: "Por favor, introduzca el nombre del colaborador"
    });
  }

  // find category and update
  Collaborators.findOneAndUpdate(
    req.params.id,
    {
      $set: req.body
    },
    { new: true }
  )
    .then(data => {
      if (!data) {
        return res.status(404).send({
          success: false,
          message: "Colaborador no encontrado con el id " + req.params.id
        });
      }
      res.send({
        success: true,
        data: data
      });
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          success: false,
          message: "Colaborador no encontrado con el id " + req.params.id
        });
      }
      return res.status(500).send({
        success: false,
        message: "Error actualizando el colaborador con el id " + req.params.id
      });
    });
};

// delete a collaborator with the specified id.
exports.collaborator_delete = (req, res) => {
  Collaborators.findOneAndDelete(req.params.id)
    .then(data => {
      if (!data) {
        return res.status(404).send({
          success: false,
          message: "Colaborador no encontrado con el id " + req.params.id
        });
      }
      res.send({
        success: true,
        message: "Collaborator successfully deleted!"
      });
    })
    .catch(err => {
      if (err.kind === "ObjectId" || err.name === "NotFound") {
        return res.status(404).send({
          success: false,
          message: "Colaborador no encontrado con el id " + req.params.id
        });
      }
      return res.status(500).send({
        success: false,
        message: "No se pudo borrar el colaborador con el id " + req.params.id
      });
    });
};
