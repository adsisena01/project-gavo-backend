// include product model
const Product = require("../models/Product.model");

// create a new Product.
exports.product_create = function (req, res) {
  //.............................. validate request.......................................................
  if (!req.body.name || !req.body.price) {
    return res.status(400).send({
      success: false,
      message: "Please enter product name and price"
    });
  }

  // create a product
  let product = new Product(
    ({ name, price, description, imageUrl, category, ingredients } = req.body)
  );

  //............................save product in the database.........................................................
  product
    .save()
    .then(data => {
      res.send({
        success: true,
        message: "Product successfully created",
        data: data
      });
    })
    .catch(err => {
      res.status(500).send({
        success: false,
        message:
          err.message || "Some error occurred while creating the product.",
      });
      console.log(err)
    });
};

//......................... retrieve and return all products.................................................................
exports.all_products = (req, res) => {
  Product.find().populate({path:'ingredients', select:'name'}).populate({path:'category', select:'name'})
    .then(data => {
      var message = "";
      if (data === undefined || data.length == 0) message = "No product found!";
      else message = "Products successfully retrieved";

      res.send({
        success: true,
        message: message,
        data: data
      });
    })
    .catch(err => {
      res.status(500).send({
        success: false,
        message: err.message || "Some error occurred while retrieving products."
      });
    });
};

// find a single product with a id.
exports.product_details = (req, res) => {
  Product.findById(req.params.id)
    .then(data => {
      if (!data) {
        return res.status(404).send({
          success: false,
          message: "Product not found with id " + req.params.id
        });
      }
      res.send({
        success: true,
        message: "Product successfully retrieved",
        data: data
      });
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          success: false,
          message: "Product not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        success: false,
        message: "Error retrieving product with id " + req.params.id
      });
    });
};

// update a product  by the id.
exports.product_update = (req, res) => {
  // validate request
  if (!req.body.title || !req.body.price) {
    return res.status(400).send({
      success: false,
      message: "Please enter product name and price"
    });
  }

  // find product and update
  Product.findOneAndUpdate(
    req.params.id,
    {
      $set: req.body
    },
    { new: true }
  )
    .then(data => {
      if (!data) {
        return res.status(404).send({
          success: false,
          message: "Product not found with id " + req.params.id
        });
      }
      res.send({
        success: true,
        data: data
      });
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          success: false,
          message: "Product not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        success: false,
        message: "Error updating product with id " + req.params.id
      });
    });
};

// delete a product with the specified id.
exports.product_delete = (req, res) => {
  Product.findOneAndDelete(req.params.id)
    .then(data => {
      if (!data) {
        return res.status(404).send({
          success: false,
          message: "Product not found with id " + req.params.id
        });
      }
      res.send({
        success: true,
        message: "Product successfully deleted!"
      });
    })
    .catch(err => {
      if (err.kind === "ObjectId" || err.name === "NotFound") {
        return res.status(404).send({
          success: false,
          message: "Product not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        success: false,
        message: "Could not delete product with id " + req.params.id
      });
    });
};

/* const productCtrl = {};
const { Product } = require("../models/product.model");

//const { userAuth } = require("../utils/Auth");

productCtrl.getProducts = async (req, res) => {
  const products = await Product.find();
  res.json(products);
};

module.exports = productCtrl; */

/* exports.getAddProduct = (req, res, next) => {
  res.render("admin/edit-product", {
    pageTitle: "Add Product",
    path: "/admin/add-product",
    editing: false,
    hasError: false,
    errorMessage: null,
    validationErrors: []
  });
};




router.post("/register-user", async (req, res) => {
    await userRegister(req.body, "user", res);
  });
 */

/* productCtrl.createProducts = async (req, res) => {
  const { title, price, description, imageUrl, userId } = req.body;
  const newProduct = new Product({
    title,
    price,
    description,
    imageUrl,
    userId
  });
  await newProduct.save();
  res.json("Nuevo producto agregado");
}; */
