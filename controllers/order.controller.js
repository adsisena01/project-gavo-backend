const { Order, ProductCart } = require('../models/order.model');


exports.getOrderById = (req, res, next, id) => {
    Order.findById(id)
        .populate("products.product", "name, price")
        .exec((err, order) => {
            if (err) {
                return res.status(400).json({
                    error: "NO order found in DB"
                });
            }
            req.order = order;
            next();
        });
};


/* exports.createOrder = (req, res) => {
    const order = new Order(req.body.order);
    order.save((err, order) => {
        if (err) {
            return res.status(400).json({
                error: "Failed to save your order in DB"
            });
        }
        res.json(order);
    });
}; */




exports.createOrder = function (req, res) {
    if (!req.body.products || !req.body.user) {
        return res.status(400).send({
            success: false,
            message: "Por favor ingrese producto y usuario "
        });
    }

    let order = new Order(
        ({ products, transaction_id, amount, address, status, updated, user } = req.body)
    )

    order
        .save()
        .then(data => {
            res.send({
                success: true,
                message: "Orden creada exitosamente",
                data: data
            });
        })
        .catch(err => {
            res.status(500).send({
                success: false,
                message:
                    err.message || "Algun error a ocurrido en la creacion de la orden."
            });
        });


};


exports.getAllOrders = (req, res) => {
    Order.find()
        .populate("user", "_id name").populate("products.0.product")
        .exec((err, order) => {
            if (err) {
                return res.status(400).json({
                    error: "No orders found in DB"
                });
            }
            res.json(order);
        });
};

exports.updateStock = (req, res, next) => {
    let myOperations = req.body.order.products.ingredients.quantity.map(prod => {
        return {
            updateOne: {
                filter: { _id: prod._id },
                update: { $inc: { quantity: -prod.count } }
            }
        };
    });

}

exports.orderReport = (req, res, next) => {
    Order.aggregate()
        .exec((err, order) => {
            if (err) {
                return res.status(400).json({
                    error: "No orders found in DB"
                });
            }
            res.json(order);
        });
}