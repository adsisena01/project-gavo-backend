const router = require("express").Router();

//......Bring in the user registration function
const {
  userRegister,
  userLogin,
  userAuth,
  checkRole,
  obtenerUsuarios
} = require("../utils/Auth");

//..........................................................................................................

//......................Users Registration Route.........................................................................
router.post("/register-user", async (req, res) => {
  await userRegister(req.body, "user", res);
});

//Admin Registration Route
router.post("/register-admin", async (req, res) => {
  await userRegister(req.body, "admin", res);
});

//Super Admin Registration Route
router.post("/register-superadmin", async (req, res) => {
  await userRegister(req.body, "superadmin", res);
});

//......Users Login Route....................................................................................................

//Users Login Route
router.post("/login-user", async (req, res) => {
  await userLogin(req.body, "user", res);
});
//Admin Login Route
router.post("/login-admin", async (req, res) => {
  await userLogin(req.body, "admin", res);
});
//Super Admin Login Route
router.post("/login-superadmin", async (req, res) => {
  await userLogin(req.body, "superadmin", res);
});

//......Profile Router....................................................................................................
router.get("/profile", userAuth, (req, res) => {
  return res.json("heloooooooooooooBBMND");
});

router.get('/list-users', ((req, res) => {
  return obtenerUsuarios()
    .then(usuarios => {
      res.json(usuarios)
    })
}))


//......Users Protected Route....................................................................................................

//Users Protected Route
router.post(
  "/user-protected",
  userAuth,
  checkRole(["user"]),
  async (req, res) => { }
);
//Admin Protected Route
router.post(
  "/admin-protected",
  userAuth,
  checkRole(["admin"]),
  async (req, res) => { }
);
//Super Admin Protected Route
router.post(
  "/super-admin-protected",
  userAuth,
  checkRole(["superadmin"]),
  async (req, res) => { }
);

//..........................................................................................................

module.exports = router;
