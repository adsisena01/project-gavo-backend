const express = require('express')

const StockCtrl = require('../controllers/stock.controller')

const router = express.Router()

router.post('/create', StockCtrl.createStock)
router.put('/update/:id', StockCtrl.updateStock)
router.delete('/delete/:id', StockCtrl.deleteStock)
router.get('/details/:id', StockCtrl.getstockById)
router.get('/list', StockCtrl.getStocks)

module.exports = router;