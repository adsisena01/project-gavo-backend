const express = require("express");
const router = express.Router();
//const { checkRole, userAuth } = require('../utils/Auth');
const { createOrder, updateStock, getAllOrders, orderReport } = require('../controllers/order.controller');

router.route("/list").get(getAllOrders)
router.route("/reports").get(orderReport)
router.route("/create/:userId").post(createOrder, updateStock)

module.exports = router;    