const express = require("express");
const router = express.Router();

//..............Import the controllers...................................
const collaborator_controller = require("../controllers/collaborators.controller");

//const { userAuth, checkRole } = require("../utils/Auth");

router.route("/list").get(collaborator_controller.all_collaborator);

router.route("/details/:id").get(collaborator_controller.collaborators_details);

router.route("/create").post(collaborator_controller.collaborators_create);

router.route("/update/:id").put(collaborator_controller.collaborator_update);

router.route("/delete/:id").delete(collaborator_controller.collaborator_delete);

module.exports = router;
