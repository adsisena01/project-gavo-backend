const express = require("express");
const router = express.Router();

//..............Import the controllers...................................
const employee_controller = require("../controllers/employee.controller");

const { checkRole, userAuth } = require("../utils/Auth");

router.route("/list").get( employee_controller.all_employee);

router.route("/details/:id").get(userAuth, checkRole('admin'),employee_controller.employees_details);

router.route("/create").post(employee_controller.employee_create);

router.route("/update/:id").put(employee_controller.employee_update);

router.route("/delete/:id").delete(employee_controller.employee_delete);

module.exports = router;
