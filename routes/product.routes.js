const express = require("express");
const router = express.Router();

//..............Import the controllers...................................
const product_controller = require("../controllers/product.controller");

const { checkRole, userAuth } = require("../utils/Auth");

router.route("/list").get( product_controller.all_products);

router.route("/details/:id").get(userAuth, checkRole('admin'),product_controller.product_details);

router.route("/create").post(product_controller.product_create);

router.route("/update/:id").put(product_controller.product_update);

router.route("/delete/:id").delete(product_controller.product_delete);

module.exports = router;
