const express = require("express");
const router = express.Router();

//..............Import the controllers...................................
const category_controller = require("../controllers/category.controller");

//const { userAuth, checkRole } = require("../utils/Auth");

router.route("/list").get(category_controller.all_category);

router.route("/details/:id").get(category_controller.category_details);

router.route("/create").post(category_controller.category_create);

router.route("/update/:id").put(category_controller.category_update);

router.route("/delete/:id").delete(category_controller.category_delete);

module.exports = router;
