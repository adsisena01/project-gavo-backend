const cors = require("cors");
const express = require("express");
const bodyParser = require("body-parser");
const morgan = require('morgan');
const passport = require("passport");
const { connect } = require("mongoose");
const { success, error } = require("consola");

//bring in the app constant
const { DB, PORT } = require("./config");

//initialize the application
const app = express();

//middlewares
app.use(morgan('dev'));
app.use(cors());
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.urlencoded({ extended: false }));

//require("./middlewares/passport")(passport);


//User Router Middleware
app.use("/api/users", require("./routes/user.routes"));
app.use("/api/employees", require("./routes/employee.routes"));
app.use("/api/pro", require("./routes/product.routes"));
app.use("/api/cat", require("./routes/category.routes"));
app.use("/api/order", require("./routes/order.routes"));
app.use("/api/stock", require("./routes/stock.routes"));
app.use("/api/collaborator", require("./routes/collaborator.routes"));



const startApp = async () => {
  try {
    //Connection With DB
    await connect(DB, {
      useFindAndModify: false,
      useCreateIndex: true,
      useUnifiedTopology: true,
      useNewUrlParser: true
    });

    success({
      message: `Conectado a la base de datos exitosamente \n${DB}`,
      badge: true
    });
    //Start Listenting for the server on port
    app.listen(PORT, () =>
      success({
        message: `Server started on PORT ${PORT}`,
        badge: true
      })
    );
  } catch (err) {
    error({
      message: `Unable to connect with the Database \n${err}`,
      badge: true
    });
    startApp();
  }
};

startApp();
